/*
 TRIDIAGLU: Function to simulate the parallel LU
 decomposition of a tridiagonal system defined by
 [a,b,c] with n number of points (per processor)
 and p processors.

 Input Arguments:
   n: number of points per processor
   p: number of processors
   a: sub-diagonal elements   (n x p)
   b: diagonal elements       (n x p)
   c: super-diagonal elements (n x p)

 Output:
   ra,rb,rc: the diagonals of the reduced system 
             resulting from the LU decomposition
              (size p-1)
*/

void TridiagLU(double *a,double *b,double *c,int n,int p,double *ra,double *rb,double *rc)
{
  int rank,i;

  /* Stage 1: Embarassingly parallel elimination of interior points */
  for (rank=0; rank<p; rank++) {
    int istart, iend;
    istart = ( rank==0 ? 1 : 2);
    iend   = n;
    for (i=istart; i<iend; i++) {
      double factor;
      factor = *(a+rank*n+i) / *(b+rank*n+i-1);
      *(b+rank*n+i) -= (factor * *(c+rank*n+i-1));
      *(a+rank*n+i) = -factor * *(a+rank*n+i-1);
      if (rank) {
        factor = *(c+rank*n) / *(b+rank*n+i-1);
        *(c+rank*n) = -factor * *(c+rank*n+i-1);
        *(b+rank*n) -= (factor * *(a+rank*n+i-1));
      }
    }
  }

  /* Stage 2: Eliminate first row on each rank except first one */
  for (rank=1; rank<p; rank++) {
    double am1, bm1, cm1, factor;
    am1 = *(a+(rank-1)*n+n-1);
    bm1 = *(b+(rank-1)*n+n-1);
    cm1 = *(c+(rank-1)*n+n-1);

    factor = *(a+rank*n) / bm1;
    *(b+rank*n) -= (factor * cm1);
    *(a+rank*n) = -factor * am1;

    factor = *(c+rank*n) / *(b+rank*n+n-1);
    *(b+rank*n) -= (factor * *(a+rank*n+n-1));
    *(c+rank*n) = -factor * *(c+rank*n+n-1);
  }

  /* stage 3: assemble the reduced system */
  for (rank=0; rank<p-1; rank++) {
    *(ra+rank) = *(a+(rank+1)*n);
    *(rb+rank) = *(b+(rank+1)*n);
    *(rc+rank) = *(c+(rank+1)*n);
  }
  *(ra) = *(rc+p-2) = 0.0;

  return;
}
