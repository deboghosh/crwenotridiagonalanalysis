#include <stdlib.h>
#include <stdio.h>

void SineWave           (int,double*,double*);
void DiscontinuousWaves (int,double*,double*);
void SumOfSines         (int,double*,double*);

int main(int argc,char **args)
{
  int nglobal = 1024;
  int ghosts  = 3;
  FILE *out;
  int i;

  double *x, *U;
  x = (double*) calloc (nglobal+2*ghosts,sizeof(double));
  U = (double*) calloc (nglobal+2*ghosts,sizeof(double));


  /* generate the discontinuous waves */
  DiscontinuousWaves(nglobal,U,x);
  out = fopen("solution_discwaves.dat","w");
  for (i=0; i<nglobal; i++) fprintf(out,"%1.16E %1.16E\n",*(x+i+ghosts),*(U+i+ghosts));
  fclose(out);

  /* generate the sum-of-sine waves */
  SumOfSines(nglobal,U,x);
  out = fopen("solution_sumofsines.dat","w");
  for (i=0; i<nglobal; i++) fprintf(out,"%1.16E %1.16E\n",*(x+i+ghosts),*(U+i+ghosts));
  fclose(out);

  free(x);
  free(U);
  return(0);
}
