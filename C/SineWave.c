/*
  Generates a function that is a sine wave
   Input arguments:
       nglobal: global  number of points

   Output:
       U: the generated function with dimensions (nglobal+2*ghosts)
       x: the 1D grid
*/

#include <math.h>

void SineWave(int nglobal,double *U,double *x)
{
  int i;

  /* some constants */
  int     ghosts  = 3;
  double  pi      = 4.0*atan(1.0);

  /* calculate dx and set grid */
  double dx = 1.0 / ((double) (nglobal-1));
  for (i=0; i<nglobal+2*ghosts; i++) *(x+i) = (i-ghosts)*dx - 0.5;

  /* calculate U */
  for (i=0; i<nglobal+2*ghosts; i++) *(U+i) = cos(2*pi*(*(x+i)));

  return;
}
