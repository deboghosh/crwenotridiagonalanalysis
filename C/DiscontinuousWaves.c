/*
  Generates a function that has various discontinuities
   Input arguments:
       nglobal: global  number of points

   Output:
       U: the generated function with dimensions (nglobal+2*ghosts)
       x: the 1D grid
*/

#include <math.h>

static double absolute(double x)
{
  return( x>0 ? x : -x);
}

void DiscontinuousWaves(int nglobal,double *U,double *x)
{
  int i;

  /* some constants */
  int ghosts = 3;

  /* calculate dx and set grid */
  double dx = 2.0 / ((double) (nglobal-1));
  for (i=0; i<nglobal+2*ghosts; i++) *(x+i) = (i-ghosts)*dx - 1.0;

  /* calculate U */
  for (i=0; i<nglobal+2*ghosts; i++) {
    if      (*(x+i) < -0.8)   *(U+i) = 0.0;
    else if (*(x+i) < -0.6)   
      *(U+i) = exp(-log(2.0)*((*(x+i)+0.7)*(*(x+i)+0.7))/0.0009);
    else if (*(x+i) < -0.4)   
      *(U+i) = 0.0;
    else if (*(x+i) < -0.2)   
      *(U+i) = 1.0;
    else if (*(x+i) <  0.0)   
      *(U+i) = 0.0;
    else if (*(x+i) <  0.2)   
      *(U+i) = 1.0-absolute(10.0*(*(x+i)-0.1));
    else if (*(x+i) <  0.4)   
      *(U+i) = 0.0;
    else if (*(x+i) <  0.6)   
      *(U+i) = sqrt(1.0-100*(*(x+i)-0.5)*(*(x+i)-0.5));
    else *(U+i) = 0.0;
  }

  return;
}
