static char help[] = "Code to analyze the spectral radius and number of iterations \nrequired for machine zero convergence of the reduced system \nresulting from the parallel LU decomposition of the CRWENO5 \ntridiagonal system.";

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <petscsys.h>

void SineWave           (int,double*,double*);
void DiscontinuousWaves (int,double*,double*);
void SumOfSines         (int,double*,double*);

void CRWENO5    (int,int,int,double*,double*,double*,double*);
void TridiagLU  (double*,double*,double*,int,int,double*,double*,double*);

void JacobiSpectralRadius(double*,double*,double*,int,double*,int);

static void GetFilename   (char*,int,char*);
static void AverageMinMax (double*,int,double*,double*,double*);

int main(int argc,char **args)
{
  PetscInitialize(&argc,&args,(char*)0,help);

  int max_levels    = 2;
  int proc_levels   = 5;
  int nrealizations = 10;
  int conv_crit     = 10;
  int power         = 100;

  FILE *in;
  in=fopen("input","r");
  if (in) {
    fscanf(in,"%d %d %d %d %d",&max_levels,&proc_levels,
           &nrealizations,&conv_crit,&power);
    fclose(in);
  }

  int nglobal_min = 256;
  int nlocal_max  = 64;

  int ghosts = 3, m, n, nglobal;

  printf("Analyzing the linear CRWENO5 scheme.\n");
  nglobal = nglobal_min;
  for (m=0; m<max_levels; m++) {
    /* figure out filename */
    char filename[100];
    GetFilename("linearwave",nglobal,filename);
    /* open file to write data to */
    FILE *out;
    out = fopen(filename,"w");
    /* analyze the system for increasing number of processors */
    int nproc = nglobal / nlocal_max;
    for (n=0; n<proc_levels; n++) {
      int nlocal = nglobal / nproc;
      printf("\t%5d\t%5d\t%5d\t",nglobal,nproc,nlocal);
      /* generate a function */
      double *x, *U;
      x = (double*) calloc (nglobal+2*ghosts,sizeof(double));
      U = (double*) calloc (nglobal+2*ghosts,sizeof(double));
      SineWave(nglobal,U,x);
      /* construct the CRWENO5 tridiagonal scheme */
      double *A,*B,*C;
      A = (double*) calloc (nlocal*nproc,sizeof(double));
      B = (double*) calloc (nlocal*nproc,sizeof(double));
      C = (double*) calloc (nlocal*nproc,sizeof(double));
      CRWENO5(nlocal,nproc,1,U,A,B,C);
      /* compute the reduced system */
      double *ra,*rb,*rc;
      ra = (double*) calloc (nproc-1,sizeof(double));
      rb = (double*) calloc (nproc-1,sizeof(double));
      rc = (double*) calloc (nproc-1,sizeof(double));
      TridiagLU(A,B,C,nlocal,nproc,ra,rb,rc);
      /* analyze the reduced system and calculate the spectral radius*/
      double rho;
      JacobiSpectralRadius(ra,rb,rc,nproc-1,&rho,power);
      /* calculate convergence rate and number of iterations */
      double  rate  = -log10(rho);
      double  niter = (conv_crit/rate);
      /* write the values to file */
      fprintf(out,"%5d\t%5d\t%1.16E\t%1.16E\n",nlocal,nproc,rho,niter);
      /* clean up */
      free(x);
      free(U);
      free(A);
      free(B);
      free(C);
      free(ra);
      free(rb);
      free(rc);
      printf("done.\n");
      /* next number of processors */
      nproc *= 2;
    }
    fclose(out);
    /* next grid size */
    nglobal *= 2;
  }

#if 0
  printf("Analyzing the CRWENO5 scheme with discontinuous waves.\n");
  nglobal = nglobal_min;
  for (m=0; m<max_levels; m++) {
    /* figure out filename */
    char filename[100];
    GetFilename("discwaves",nglobal,filename);
    /* open file to write data to */
    FILE *out;
    out = fopen(filename,"w");
    /* analyze the system for increasing number of processors */
    int nproc = nglobal / nlocal_max;
    for (n=0; n<proc_levels; n++) {
      int nlocal = nglobal / nproc;
      printf("\t%5d\t%5d\t%5d\t",nglobal,nproc,nlocal);
      /* generate a function */
      double *x, *U;
      x = (double*) calloc (nglobal+2*ghosts,sizeof(double));
      U = (double*) calloc (nglobal+2*ghosts,sizeof(double));
      DiscontinuousWaves(nglobal,U,x);
      /* construct the CRWENO5 tridiagonal scheme */
      double *A,*B,*C;
      A = (double*) calloc (nlocal*nproc,sizeof(double));
      B = (double*) calloc (nlocal*nproc,sizeof(double));
      C = (double*) calloc (nlocal*nproc,sizeof(double));
      CRWENO5(nlocal,nproc,0,U,A,B,C);
      /* compute the reduced system */
      double *ra,*rb,*rc;
      ra = (double*) calloc (nproc-1,sizeof(double));
      rb = (double*) calloc (nproc-1,sizeof(double));
      rc = (double*) calloc (nproc-1,sizeof(double));
      TridiagLU(A,B,C,nlocal,nproc,ra,rb,rc);
      /* analyze the reduced system and calculate the spectral radius*/
      double rho;
      JacobiSpectralRadius(ra,rb,rc,nproc-1,&rho,power);
      /* calculate convergence rate and number of iterations */
      double  rate  = -log10(rho);
      double  niter = (conv_crit/rate);
      /* write the values to file */
      fprintf(out,"%5d\t%5d\t%1.16E\t%1.16E\n",nlocal,nproc,rho,niter);
      /* clean up */
      free(x);
      free(U);
      free(A);
      free(B);
      free(C);
      free(ra);
      free(rb);
      free(rc);
      printf("done.\n");
      /* next number of processors */
      nproc *= 2;
    }
    fclose(out);
    /* next grid size */
    nglobal *= 2;
  }

  printf("Analyzing the CRWENO5 scheme with sum of sine waves.\n");
  nglobal = nglobal_min;
  for (m=0; m<max_levels; m++) {
    /* figure out filename */
    char filename[100];
    GetFilename("sumofsines",nglobal,filename);
    /* open file to write data to */
    FILE *out;
    out = fopen(filename,"w");
    /* analyze the system for increasing number of processors */
    int nproc = nglobal / nlocal_max;
    for (n=0; n<proc_levels; n++) {
      int nlocal = nglobal / nproc, i;
      printf("\t%5d\t%5d\t%5d\t",nglobal,nproc,nlocal);
      double *rho = (double*) calloc (nrealizations,sizeof(double));
      for (i=0; i<nrealizations; i++) {
        /* generate a function */
        double *x, *U;
        x = (double*) calloc (nglobal+2*ghosts,sizeof(double));
        U = (double*) calloc (nglobal+2*ghosts,sizeof(double));
        SumOfSines(nglobal,U,x);
        /* construct the CRWENO5 tridiagonal scheme */
        double *A,*B,*C;
        A = (double*) calloc (nlocal*nproc,sizeof(double));
        B = (double*) calloc (nlocal*nproc,sizeof(double));
        C = (double*) calloc (nlocal*nproc,sizeof(double));
        CRWENO5(nlocal,nproc,0,U,A,B,C);
        /* compute the reduced system */
        double *ra,*rb,*rc;
        ra = (double*) calloc (nproc-1,sizeof(double));
        rb = (double*) calloc (nproc-1,sizeof(double));
        rc = (double*) calloc (nproc-1,sizeof(double));
        TridiagLU(A,B,C,nlocal,nproc,ra,rb,rc);
        /* analyze the reduced system and calculate the spectral radius*/
        JacobiSpectralRadius(ra,rb,rc,nproc-1,(rho+i),power);
        /* clean up */
        free(x);
        free(U);
        free(A);
        free(B);
        free(C);
        free(ra);
        free(rb);
        free(rc);
        if (i%(nrealizations/4) == 0) printf(".");
      }
      /* calculate average, min and max spectral radius */
      double rho_avg, rho_min, rho_max;
      AverageMinMax(rho,nrealizations,&rho_avg,&rho_min,&rho_max);
      /* calculate convergence rate and number of iterations */
      double rate;
      rate = -log10(rho_avg);
      double niter_avg = (conv_crit/rate);
      rate = -log10(rho_min);
      double niter_min = (conv_crit/rate);
      rate = -log10(rho_max);
      double niter_max = (conv_crit/rate);
      /* write the values to file */
      fprintf(out,"%5d %5d %1.16E %1.16E %1.16E %1.16E %1.16E %1.16E\n",nlocal,nproc,
              rho_avg, rho_min, rho_max,niter_avg, niter_min, niter_max);
      /* clean up */
      printf("\tdone.\n");
      /* next number of processors */
      nproc *= 2;
    }
    fclose(out);
    /* next grid size */
    nglobal *= 2;
  }
#endif

  PetscFinalize();
  return(0);
}

void AverageMinMax(double *x,int n,double *xavg,double *xmin,double *xmax)
{
  *xavg = 0;
  *xmax = -1e300;
  *xmin = +1e300;
  int i;
  for (i=0; i<n; i++) {
    *xavg += *(x+i);
    if (*(x+i) < *xmin) *xmin = *(x+i);
    if (*(x+i) > *xmax) *xmax = *(x+i);
  }
  *xavg /= n;
}

void GetStringFromInteger(int a,char *A,int width)
{
  int i;
  for (i=0; i<width; i++) {
    char digit = (char) (a%10 + '0'); 
    a /= 10;
    A[width-1-i] = digit;
  }
  return;
}

void GetFilename(char *root,int N,char *filename)
{
  char  tail[50]="";

  GetStringFromInteger(N,tail,6);
  strcpy(filename,"");
  strcat(filename,root);
  strcat(filename,"." );
  strcat(filename,tail);

  return;
}
