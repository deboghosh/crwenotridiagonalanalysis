#include <stdlib.h>
#include <petscmat.h>

void JacobiSpectralRadius(double *a,double *b,double *c,int n,double *rho,int k1)
{
  Mat   G, A, C;
  int   i,its,nconv;

  /* threshold */
  double eps = 1e-40;

  /* create the matrix of size nxn */
  MatCreate(PETSC_COMM_WORLD,&G);
  MatSetSizes(G,PETSC_DECIDE,PETSC_DECIDE,n,n);
  MatSetFromOptions(G);
  MatSetUp(G);
  MatCreate(PETSC_COMM_WORLD,&A);
  MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,n,n);
  MatSetFromOptions(A);
  MatSetUp(A);

  /* set the values of the matrix corresponding to the Jacobi method */
  for (i=0; i<n; i++) {
    if (i == 0) {
      /* first row */
      double  value  = -c[i]/b[i];
      int     col    = i+1;
      MatSetValues(G,1,&i,1,&col,&value,INSERT_VALUES);
    } else if (i == n-1) {
      /* last row */
      double  value  = -a[i]/b[i];
      int     col    = i-1;
      MatSetValues(G,1,&i,1,&col,&value,INSERT_VALUES);
    } else {
      /* interior */
      double  value[3]  = {-a[i]/b[i] ,0.0,-c[i]/b[i]};
      int     col[3]    = {i-1        ,i  ,i+1       };
      MatSetValues(G,1,&i,3,col,value,INSERT_VALUES);
    }
  }
  MatAssemblyBegin(G,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd  (G,MAT_FINAL_ASSEMBLY);
  /* initialize the A = G^k matrix as G */
  MatCopy(G,A,DIFFERENT_NONZERO_PATTERN);

  /* upper bound on k based on n */
  int kmax = (int) (k1*log(n)/log(2));

  /* compute A = G^k */
  int k = 1;
  /* part one: fast k = 1,2,4,8,16... */
  while(k < kmax) {
    MatMatMult(A, A, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&C);
    double norm; MatNorm(C,NORM_FROBENIUS,&norm);
    MatCopy(C,A,DIFFERENT_NONZERO_PATTERN); MatDestroy(&C);
    k *= 2;
    if (norm < eps) break;
  }

  /* calculate inf norm of A = G^k */
  double norm;
  MatNorm(A,NORM_FROBENIUS,&norm);

  /* calculate spectral radius estimate */
  *rho = exp(log(norm)/k);

  MatDestroy(&G);
  MatDestroy(&A);
  return;
}
