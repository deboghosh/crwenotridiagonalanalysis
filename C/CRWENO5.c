#include <stdlib.h>

/*
CRWENO5: Function to create the left-hand side tridiagonal
         matrix for the CRWENO5 scheme

 Input arguments:
   nlocal: local number of points on each processor
   nproc : number of processors
   flag  : if 1, use optimal weights, if 0, use random weights
   U     : a function, U(x), based on which the weights are computed

 Output:
  A,B,C  : the three diagonals of the tridiagonal system of equations
*/

void CRWENO5(int nlocal,int nproc,int flag,double *U,double *A,double *B,double *C)
{
  int rank, i;

  /* optimal coefficients */
  double c1, c2, c3;
  c1 = 0.2;
  c2 = 0.5;
  c3 = 0.3;

  /* some constants */
  double eps    = 0.000001;
  int    ghosts = 3;

  /* finding local u from global U */
  double *u = (double*) calloc(nproc*(nlocal+2*ghosts),sizeof(double));
  for (rank=0; rank<nproc; rank++) {
    for (i=0; i<(nlocal+2*ghosts); i++) {
      *(u+rank*(nlocal+2*ghosts)+i) = *(U+rank*nlocal+i);
    }
  }

  /* allocate arrays for the tridiagonal system of the interface flux */
  double *alpha, *beta, *gamma;
  alpha = (double*) calloc (nproc*(nlocal+1),sizeof(double));
  beta  = (double*) calloc (nproc*(nlocal+1),sizeof(double));
  gamma = (double*) calloc (nproc*(nlocal+1),sizeof(double));

  /* create the CRWENO5 interface tridiagonal system */
  for (rank=0; rank<nproc; rank++) {
    for (i=0; i<nlocal+1; i++) {
      /* setting WENO weights */
      double w1, w2, w3;
      if (flag) {
        w1 = c1;
        w2 = c2;
        w3 = c3;
      } else {
        double m3, m2, m1, p1, p2;
        m3 = *(u+rank*(nlocal+2*ghosts)+i-3+ghosts);
        m2 = *(u+rank*(nlocal+2*ghosts)+i-2+ghosts);
        m1 = *(u+rank*(nlocal+2*ghosts)+i-1+ghosts);
        p1 = *(u+rank*(nlocal+2*ghosts)+i  +ghosts);
        p2 = *(u+rank*(nlocal+2*ghosts)+i+1+ghosts);
        double b1, b2, b3;
        b1 = 13.0/12.0*(m3-2*m2+m1)*(m3-2*m2+m1) 
              + 1.0/4.0*(m3-4*m2+3*m1)*(m3-4*m2+3*m1);
        b2 = 13.0/12.0*(m2-2*m1+p1)*(m2-2*m1+p1) 
              + 1.0/4.0*(m2-p1)*(m2-p1);
        b3 = 13.0/12.0*(m1-2*p1+p2)*(m1-2*p1+p2) 
              + 1.0/4.0*(3*m1-4*p1+p2)*(3*m1-4*p1+p2);
        double tau = (m3-4*m2+6*m1-4*p1+p2)*(m3-4*m2+6*m1-4*p1+p2);
        double a1, a2, a3;
        a1 = c1 * (1.0 + (tau/(b1+eps))*(tau/(b1+eps)));
        a2 = c2 * (1.0 + (tau/(b2+eps))*(tau/(b2+eps)));
        a3 = c3 * (1.0 + (tau/(b3+eps))*(tau/(b3+eps)));
        w1 = a1 / (a1+a2+a3);
        w2 = a2 / (a1+a2+a3);
        w3 = a3 / (a1+a2+a3);
      }
      /* left-hand-side coefficients of CRWENO5 scheme */
      *(alpha+rank*(nlocal+1)+i) = (2.0/3.0)*w1 + (1.0/3.0)*w2;
      *(beta +rank*(nlocal+1)+i) = (1.0/3.0)*w1 + (2.0/3.0)*(w2+w3);
      *(gamma+rank*(nlocal+1)+i) = (1.0/3.0)*w3;
    }
  }
  /* use WENO5 at the physical boundaries */
  alpha[0] = 0.0;
  beta [0] = 1.0;
  gamma[0] = 0.0;
  *(alpha+(nproc-1)*(nlocal+1)+nlocal) = 0.0;
  *(beta +(nproc-1)*(nlocal+1)+nlocal) = 1.0;
  *(gamma+(nproc-1)*(nlocal+1)+nlocal) = 0.0;
  /* ensuring consistenncy for interfaces that abut subdomain boundaries */
  for (rank=0;rank<nproc-1;rank++) {
    *(alpha+rank*(nlocal+1)+nlocal) = *(alpha+(rank+1)*(nlocal+1));
    *(beta +rank*(nlocal+1)+nlocal) = *(beta +(rank+1)*(nlocal+1));
    *(gamma+rank*(nlocal+1)+nlocal) = *(gamma+(rank+1)*(nlocal+1));
  }

  /* create the derivative tridiagonal system */
  for (rank=0;rank<nproc;rank++) {
    for (i=0; i<nlocal; i++) {
      *(A+rank*nlocal+i) = *(alpha+rank*(nlocal+1)+i);
      *(B+rank*nlocal+i) =  (*(beta +rank*(nlocal+1)+i  ))
                           +(*(alpha+rank*(nlocal+1)+i  ))
                           -(*(alpha+rank*(nlocal+1)+i+1));
      *(C+rank*nlocal+i) =  (*(gamma+rank*(nlocal+1)+i  ))
                           +(*(alpha+rank*(nlocal+1)+i  ))
                           +(*(beta +rank*(nlocal+1)+i  ))
                           -(*(alpha+rank*(nlocal+1)+i+1))
                           -(*(beta +rank*(nlocal+1)+i+1));
    }
  }

  /* clean up */
  free(alpha);
  free(beta);
  free(gamma);
  free(u);
  return;
}
