/*
  Generates a function that is the sum of sine waves
   Input arguments:
       nglobal: global  number of points

   Output:
       U: the generated function with dimensions (nglobal+2*ghosts)
       x: the 1D grid
*/

#include <stdlib.h>
#include <math.h>

void SumOfSines(int nglobal,double *U,double *x)
{
  int i,k;

  /* some constants */
  int     ghosts  = 3;
  int     wn_max  = nglobal/2;
  double  pi      = 4.0*atan(1.0);

  /* calculate dx and set grid */
  double dx = 1.0 / ((double) (nglobal-1));
  for (i=0; i<nglobal+2*ghosts; i++) *(x+i) = (i-ghosts)*dx - 0.5;

  /* generate random phases */
  double *phase = (double*) calloc (wn_max,sizeof(double));
  srand(time(NULL));
  for (k=0; k<wn_max; k++)
    phase[k] = -pi + 2*pi*(((double)rand())/((double)RAND_MAX));

  /* calculate U */
  for (i=0; i<nglobal+2*ghosts; i++) {
    *(U+i) = 0.0;
    for (k=1; k<wn_max; k++) {
      double Ak = exp((-5.0/6.0)*log((double)k));
      *(U+i) += Ak*cos(2*pi*k*(*(x+i))+(*(phase+k)));
    }
  }

  free(phase);
  return;
}
