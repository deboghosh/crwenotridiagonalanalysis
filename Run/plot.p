set terminal postscript eps enhanced "Times" 10

set grid  xtics lt 4 lw 1 lc rgbcolor "gray"
set grid  ytics lt 4 lw 1 lc rgbcolor "gray"

set style line  1 lt 1 lw 2 lc rgbcolor "black" pt 4  ps 2.0
set style line  2 lt 1 lw 2 lc rgbcolor "black" pt 6  ps 2.0
set style line  3 lt 1 lw 2 lc rgbcolor "black" pt 8  ps 2.0
set style line  4 lt 1 lw 2 lc rgbcolor "black" pt 10 ps 2.0
set style line  5 lt 1 lw 2 lc rgbcolor "black" pt 12 ps 2.0
set style line  6 lt 1 lw 2 lc rgbcolor "black" pt 14 ps 2.0
set style line  7 lt 1 lw 2 lc rgbcolor "black" pt 40 ps 2.0

set style line 11 lt 4 lw 2 lc rgbcolor "black" pt 4  ps 1.25
set style line 12 lt 4 lw 2 lc rgbcolor "black" pt 6  ps 1.25
set style line 13 lt 4 lw 2 lc rgbcolor "black" pt 8  ps 1.25
set style line 14 lt 4 lw 2 lc rgbcolor "black" pt 10 ps 1.25
set style line 15 lt 4 lw 2 lc rgbcolor "black" pt 12 ps 1.25
set style line 16 lt 4 lw 2 lc rgbcolor "black" pt 14 ps 1.25
set style line 17 lt 4 lw 2 lc rgbcolor "black" pt 40 ps 1.25

set style line 21 lt 2 lw 2 lc rgbcolor "black" pt 5  ps 1.5
set style line 22 lt 2 lw 2 lc rgbcolor "black" pt 7  ps 1.5
set style line 23 lt 2 lw 2 lc rgbcolor "black" pt 9  ps 1.5
set style line 24 lt 2 lw 2 lc rgbcolor "black" pt 11 ps 1.5
set style line 25 lt 2 lw 2 lc rgbcolor "black" pt 13 ps 1.5
set style line 26 lt 2 lw 2 lc rgbcolor "black" pt 15 ps 1.5
set style line 27 lt 2 lw 2 lc rgbcolor "black" pt 42 ps 1.5

set style line 31 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0
set style line 32 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0
set style line 33 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0
set style line 34 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0
set style line 35 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0
set style line 36 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0
set style line 37 lt 1 lw 16 lc rgb "#888888" pt 0 ps 0

set bars 4

set output "Nproc_vs_SpectralRadius.tmp.eps"
set size ratio 0.75
set format y "%1.0e"
set logscale x
set logscale y
set xrange [4:5000]
set yrange [1e-16:1]
set xtics (4,8,16,32,64,128,256,512,1024,2048,4096)
set xlabel "Number of processors" font "Times,22"
set ylabel "Estimated spectral radius" font "Times,22"
set key bottom right
plot \
"sumofsines.000256" u 2:3:4:5 w errorbars ls 31 notitle, \
"sumofsines.000512" u 2:3:4:5 w errorbars ls 32 notitle, \
"sumofsines.001024" u 2:3:4:5 w errorbars ls 33 notitle, \
"sumofsines.002048" u 2:3:4:5 w errorbars ls 34 notitle, \
"sumofsines.004096" u 2:3:4:5 w errorbars ls 35 notitle, \
"sumofsines.008192" u 2:3:4:5 w errorbars ls 36 notitle, \
"sumofsines.016384" u 2:3:4:5 w errorbars ls 37 notitle, \
"linearwave.000256" u 2:3 w lp ls  1 notitle  , \
"discwaves.000256"  u 2:3 w lp ls 11 notitle  , \
"sumofsines.000256" u 2:3 w lp ls 21 notitle  , \
"linearwave.000512" u 2:3 w lp ls  2 notitle  , \
"discwaves.000512"  u 2:3 w lp ls 12 notitle  , \
"sumofsines.000512" u 2:3 w lp ls 22 notitle  , \
"linearwave.001024" u 2:3 w lp ls  3 notitle , \
"discwaves.001024"  u 2:3 w lp ls 13 notitle , \
"sumofsines.001024" u 2:3 w lp ls 23 notitle , \
"linearwave.002048" u 2:3 w lp ls  4 notitle , \
"discwaves.002048"  u 2:3 w lp ls 14 notitle , \
"sumofsines.002048" u 2:3 w lp ls 24 notitle , \
"linearwave.004096" u 2:3 w lp ls  5 notitle , \
"discwaves.004096"  u 2:3 w lp ls 15 notitle , \
"sumofsines.004096" u 2:3 w lp ls 25 notitle , \
"linearwave.008192" u 2:3 w lp ls  6 notitle , \
"discwaves.008192"  u 2:3 w lp ls 16 notitle , \
"sumofsines.008192" u 2:3 w lp ls 26 notitle, \
"linearwave.016384" u 2:3 w lp ls  7 notitle, \
"discwaves.016384"  u 2:3 w lp ls 17 notitle, \
"sumofsines.016384" u 2:3 w lp ls 27 notitle

set output "Nlocal_vs_SpectralRadius.tmp.eps"
set size ratio 0.75
set format y "%1.0e"
set logscale x
set logscale y
set xrange [3:80]
set yrange [1e-16:1]
set xtics (4,8,16,32,64)
set xlabel "Subdomain size" font "Times,22"
set ylabel "Estimated spectral radius" font "Times,22"
set key top right
plot \
"sumofsines.000256" u 1:3:4:5 w errorbars ls 31 notitle, \
"sumofsines.000512" u 1:3:4:5 w errorbars ls 32 notitle, \
"sumofsines.001024" u 1:3:4:5 w errorbars ls 33 notitle, \
"sumofsines.002048" u 1:3:4:5 w errorbars ls 34 notitle, \
"sumofsines.004096" u 1:3:4:5 w errorbars ls 35 notitle, \
"sumofsines.008192" u 1:3:4:5 w errorbars ls 36 notitle, \
"sumofsines.016384" u 1:3:4:5 w errorbars ls 37 notitle, \
"linearwave.000256" u 1:3 w lp ls  1 notitle, \
"discwaves.000256"  u 1:3 w lp ls 11 notitle, \
"sumofsines.000256" u 1:3 w lp ls 21 notitle, \
"linearwave.000512" u 1:3 w lp ls  2 notitle, \
"discwaves.000512"  u 1:3 w lp ls 12 notitle, \
"sumofsines.000512" u 1:3 w lp ls 22 notitle, \
"linearwave.001024" u 1:3 w lp ls  3 notitle, \
"discwaves.001024"  u 1:3 w lp ls 13 notitle, \
"sumofsines.001024" u 1:3 w lp ls 23 notitle, \
"linearwave.002048" u 1:3 w lp ls  4 notitle, \
"discwaves.002048"  u 1:3 w lp ls 14 notitle, \
"sumofsines.002048" u 1:3 w lp ls 24 notitle, \
"linearwave.004096" u 1:3 w lp ls  5 notitle, \
"discwaves.004096"  u 1:3 w lp ls 15 notitle, \
"sumofsines.004096" u 1:3 w lp ls 25 notitle, \
"linearwave.008192" u 1:3 w lp ls  6 notitle, \
"discwaves.008192"  u 1:3 w lp ls 16 notitle, \
"sumofsines.008192" u 1:3 w lp ls 26 notitle, \
"linearwave.016384" u 1:3 w lp ls  7 notitle, \
"discwaves.016384"  u 1:3 w lp ls 17 notitle, \
"sumofsines.016384" u 1:3 w lp ls 27 notitle

set output "Nproc_vs_Niter.tmp.eps"
set size ratio 0.75
set format y "%g"
set logscale x
unset logscale y
set xrange [3:5000]
set yrange [-0.5:11]
set xtics (4,8,16,32,64,128,256,512,1024,2048,4096)
set xlabel "Number of processors" font "Times,22"
set ylabel "Estimated number of iterations" font "Times,22"
set key top left
plot \
"sumofsines.000256" u 2:6:7:8 w errorbars ls 31 notitle, \
"sumofsines.000512" u 2:6:7:8 w errorbars ls 32 notitle, \
"sumofsines.001024" u 2:6:7:8 w errorbars ls 33 notitle, \
"sumofsines.002048" u 2:6:7:8 w errorbars ls 34 notitle, \
"sumofsines.004096" u 2:6:7:8 w errorbars ls 35 notitle, \
"sumofsines.008192" u 2:6:7:8 w errorbars ls 36 notitle, \
"sumofsines.016384" u 2:6:7:8 w errorbars ls 37 notitle, \
"linearwave.000256" u 2:4 w lp ls  1 notitle, \
"discwaves.000256"  u 2:4 w lp ls 11 notitle, \
"sumofsines.000256" u 2:6 w lp ls 21 notitle, \
"linearwave.000512" u 2:4 w lp ls  2 notitle, \
"discwaves.000512"  u 2:4 w lp ls 12 notitle, \
"sumofsines.000512" u 2:6 w lp ls 22 notitle, \
"linearwave.001024" u 2:4 w lp ls  3 notitle, \
"discwaves.001024"  u 2:4 w lp ls 13 notitle, \
"sumofsines.001024" u 2:6 w lp ls 23 notitle, \
"linearwave.002048" u 2:4 w lp ls  4 notitle, \
"discwaves.002048"  u 2:4 w lp ls 14 notitle, \
"sumofsines.002048" u 2:6 w lp ls 24 notitle, \
"linearwave.004096" u 2:4 w lp ls  5 notitle, \
"discwaves.004096"  u 2:4 w lp ls 15 notitle, \
"sumofsines.004096" u 2:6 w lp ls 25 notitle, \
"linearwave.008192" u 2:4 w lp ls  6 notitle, \
"discwaves.008192"  u 2:4 w lp ls 16 notitle, \
"sumofsines.008192" u 2:6 w lp ls 26 notitle, \
"linearwave.016384" u 2:4 w lp ls  7 notitle, \
"discwaves.016384"  u 2:4 w lp ls 17 notitle, \
"sumofsines.016384" u 2:6 w lp ls 27 notitle

set output "Nlocal_vs_Niter.tmp.eps"
set size ratio 0.75
set format y "%g"
set logscale x
unset logscale y
set xrange [3:80]
set yrange [-0.50:11]
set xtics (4,8,16,32,64)
set xlabel "Subdomain size" font "Times,22"
set ylabel "Estimated number of iterations" font "Times,22"
set key top right
plot \
"sumofsines.000256" u 1:6:7:8 w errorbars ls 31 notitle, \
"sumofsines.000512" u 1:6:7:8 w errorbars ls 32 notitle, \
"sumofsines.001024" u 1:6:7:8 w errorbars ls 33 notitle, \
"sumofsines.002048" u 1:6:7:8 w errorbars ls 34 notitle, \
"sumofsines.004096" u 1:6:7:8 w errorbars ls 35 notitle, \
"sumofsines.008192" u 1:6:7:8 w errorbars ls 36 notitle, \
"sumofsines.016384" u 1:6:7:8 w errorbars ls 37 notitle, \
"linearwave.000256" u 1:4 w lp ls  1 notitle, \
"discwaves.000256"  u 1:4 w lp ls 11 notitle, \
"sumofsines.000256" u 1:6 w lp ls 21 notitle, \
"linearwave.000512" u 1:4 w lp ls  2 notitle, \
"discwaves.000512"  u 1:4 w lp ls 12 notitle, \
"sumofsines.000512" u 1:6 w lp ls 22 notitle, \
"linearwave.001024" u 1:4 w lp ls  3 notitle, \
"discwaves.001024"  u 1:4 w lp ls 13 notitle, \
"sumofsines.001024" u 1:6 w lp ls 23 notitle, \
"discwaves.002048"  u 1:4 w lp ls 14 notitle, \
"linearwave.002048" u 1:4 w lp ls  4 notitle, \
"sumofsines.002048" u 1:6 w lp ls 24 notitle, \
"linearwave.004096" u 1:4 w lp ls  5 notitle, \
"discwaves.004096"  u 1:4 w lp ls 15 notitle, \
"sumofsines.004096" u 1:6 w lp ls 25 notitle, \
"linearwave.008192" u 1:4 w lp ls  6 notitle, \
"discwaves.008192"  u 1:4 w lp ls 16 notitle, \
"sumofsines.008192" u 1:6 w lp ls 26 notitle, \
"linearwave.016384" u 1:4 w lp ls  7 notitle, \
"discwaves.016384"  u 1:4 w lp ls 17 notitle, \
"sumofsines.016384" u 1:6 w lp ls 27 notitle

set output "Analysis_Legend.tmp.eps"
set size ratio 1
set noborder
set noxtics
set noytics
set notitle
set noxlabel
set noylabel
set xrange [1:2]
set yrange [-100:-1]
set key below spacing 1.5 samplen 4
plot \
"sumofsines.000256" u 1:6:7:8 w errorbars ls 31 notitle, \
"sumofsines.000512" u 1:6:7:8 w errorbars ls 32 notitle, \
"sumofsines.001024" u 1:6:7:8 w errorbars ls 33 notitle, \
"sumofsines.002048" u 1:6:7:8 w errorbars ls 34 notitle, \
"sumofsines.004096" u 1:6:7:8 w errorbars ls 35 notitle, \
"sumofsines.008192" u 1:6:7:8 w errorbars ls 36 notitle, \
"sumofsines.016384" u 1:6:7:8 w errorbars ls 37 notitle, \
"linearwave.000256" u 1:4 w lp ls  1 t "Case 1: N =   256"  , \
"discwaves.000256"  u 1:4 w lp ls 11 t "Case 2: N =   256"  , \
"sumofsines.000256" u 1:6 w lp ls 21 t "Case 3: N =   256"  , \
"linearwave.000512" u 1:4 w lp ls  2 t "Case 1: N =   512"  , \
"discwaves.000512"  u 1:4 w lp ls 12 t "Case 2: N =   512"  , \
"sumofsines.000512" u 1:6 w lp ls 22 t "Case 3: N =   512"  , \
"linearwave.001024" u 1:4 w lp ls  3 t "Case 1: N =  1024" , \
"discwaves.001024"  u 1:4 w lp ls 13 t "Case 2: N =  1024" , \
"sumofsines.001024" u 1:6 w lp ls 23 t "Case 3: N =  1024" , \
"discwaves.002048"  u 1:4 w lp ls 14 t "Case 2: N =  2048" , \
"linearwave.002048" u 1:4 w lp ls  4 t "Case 1: N =  2048" , \
"sumofsines.002048" u 1:6 w lp ls 24 t "Case 3: N =  2048" , \
"linearwave.004096" u 1:4 w lp ls  5 t "Case 1: N =  4096" , \
"discwaves.004096"  u 1:4 w lp ls 15 t "Case 2: N =  4096" , \
"sumofsines.004096" u 1:6 w lp ls 25 t "Case 3: N =  4096" , \
"linearwave.008192" u 1:4 w lp ls  6 t "Case 1: N =  8192" , \
"discwaves.008192"  u 1:4 w lp ls 16 t "Case 2: N =  8192" , \
"sumofsines.008192" u 1:6 w lp ls 26 t "Case 3: N =  8192", \
"linearwave.016384" u 1:4 w lp ls  7 t "Case 1: N = 16384", \
"discwaves.016384"  u 1:4 w lp ls 17 t "Case 2: N = 16384", \
"sumofsines.016384" u 1:6 w lp ls 27 t "Case 3: N = 16384"

