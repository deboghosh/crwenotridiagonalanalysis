function [U,x] = SumOfSines(nlocal,nproc)
%SUMOFSINES Generates a function that is the sum of sine waves
%   Input arguments:
%       nlocal: number of points on each processor
%       nproc : number of processors

%   Output arguments:
%       U: the generated function with dimensions (nglobal+2*ghosts,nproc)

% some constants
ghosts = 3;

% calculate global domain size
nglobal = nlocal*nproc;

% allocate global x and U arrays
x = zeros(nglobal+2*ghosts,1);
U = zeros(nglobal+2*ghosts,1);

% calculate dx and set x (grid)
dx = 2.0 / (nglobal-1);
for i=1:(nglobal+2*ghosts)
    x(i) = (i-1-ghosts)*dx - 1.0;
end

% calculate U
for i=1:(nglobal+2*ghosts)
    if (x(i) < -0.8)
        U(i) = 0.0;
    elseif (x(i) < -0.6)
        U(i) = exp(-log(2.0)*(x(i)+0.7)^2/0.0009);
    elseif (x(i) < -0.4)
        U(i) = 0.0;
    elseif (x(i) < -0.2)
        U(i) = 1.0;
    elseif (x(i) < 0.0)
        U(i) = 0.0;
    elseif (x(i) < 0.2)
        U(i) = 1.0 - abs(10.0*(x(i)-0.1));
    elseif (x(i) < 0.4)
        U(i) = 0.0;
    elseif (x(i) < 0.6)
        U(i) = sqrt(1.0-100*(x(i)-0.5)^2);
    else
        U(i) = 0.0;
    end
end

end

