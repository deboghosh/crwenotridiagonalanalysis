clear all;
close all;

ghosts = 3;
Dmin = 8;
Dmax = 14;
conv_crit = 10;
D = Dmin:Dmax;

% style=['ko--','k^--','kv--','ks--','kd--','kh--','kp--'];
% ns1=4;
% ns2=ns1-1;
% % analyze the periodic linear CRWENO5 scheme
% for j=1:size(D,2)
%     d = D(j);
%     nglobal = 2^d;
%     nprocs  = 2.^(max(2,d-6):d-2);
%     ndata   = size(nprocs,2);
%     rho     = zeros(ndata,1);
%     niter   = zeros(ndata,1);
%     nlocal  = zeros(ndata,1);
%     for i = 1:ndata
%         nlocal(i) = nglobal/nprocs(i);
%         fprintf('%4d %4d %4d\n',nglobal,nprocs(i),nlocal(i));
%         % construct the CRWENO5 tridiagonal scheme
%         [A,B,C]= CRWENO5Periodic(nlocal(i),nprocs(i),1);
%         % compute the reduced system
%         R = TridiagLU(A,B,C,nlocal(i),nprocs(i));
%         % construct the iteration matrix for Jacobi iterations
%         G = eye(nprocs(i)-1)-diag(1./diag(R),0)*R;
%         % calculate max eigenvalue
%         rho(i) = max(abs(eig(G)));
%         % calculate convergence rate
%         rate = -log10(rho(i));
%         % estimate number of iterations for machine-zero conv
%         niter(i) = int16(16/rate);
%     end
%     figure(1);
%     loglog(nprocs,rho,style(ns1*j-ns2:ns1*j),'linewidth',2,'MarkerSize',7);
%     hold on;
%     figure(2);
%     loglog(nlocal,rho,style(ns1*j-ns2:ns1*j),'linewidth',2,'MarkerSize',7);
%     hold on;
%     figure(3);
%     semilogx(nprocs,niter,style(ns1*j-ns2:ns1*j),'linewidth',2,'MarkerSize',7);
%     hold on;
%     figure(4);
%     semilogx(nlocal,niter,style(ns1*j-ns2:ns1*j),'linewidth',2,'MarkerSize',7);
%     hold on;
% end

% analyze the aperiodic linear CRWENO5 scheme
style=['ko-','k^-','kv-','ks-','kd-','kh-','kp-'];
ns1=3;
ns2=ns1-1;
for j=1:size(D,2)
    d = D(j);
    nglobal = 2^d;
    nprocs  = 2.^(max(2,d-6):d-2);
    ndata   = size(nprocs,2);
    rho     = zeros(ndata,1);
    niter   = zeros(ndata,1);
    nlocal  = zeros(ndata,1);
    for i = 1:ndata
        nlocal(i) = nglobal/nprocs(i);
        fprintf('%4d %4d %4d\n',nglobal,nprocs(i),nlocal(i));
        % generate a zero array to pass as the function for CRWENO5
        U = zeros(nlocal(i)*nprocs(i)+2*ghosts,1);
        % construct the CRWENO5 tridiagonal scheme
        [A,B,C]= CRWENO5(nlocal(i),nprocs(i),1,U);
        % compute the reduced system
        R = TridiagLU(A,B,C,nlocal(i),nprocs(i));
        % construct the iteration matrix for Jacobi iterations
        G = eye(nprocs(i)-1)-diag(1./diag(R),0)*R;
        % calculate max eigenvalue
        rho(i) = max(abs(eig(G)));
        % calculate convergence rate
        rate = -log10(rho(i));
        % estimate number of iterations for machine-zero conv
        niter(i) = int16(conv_crit/rate);
    end
    figure(1);
    loglog(nprocs,rho,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
           'MarkerSize',15);
    hold on;
    figure(2);
    loglog(nlocal,rho,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
           'MarkerSize',15);
    hold on;
    figure(3);
    semilogx(nprocs,niter,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
             'MarkerSize',15);
    hold on;
    figure(4);
    semilogx(nlocal,niter,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
             'MarkerSize',15);
    hold on;
end

figure(1);
ylabel('Spectral Radius','FontWeight','demi','FontSize',18,'FontName','Times');
xlabel('Number of processors','FontWeight','demi','FontSize',18,'FontName','Times');
grid on;
axis([4 5000 1e-16 1]);
set(gca,'FontSize',16,'FontName','Times');
hold on;
figure(2);
ylabel('Spectral Radius','FontWeight','demi','FontSize',18,'FontName','Times');
xlabel('Sub-domain size','FontWeight','demi','FontSize',18,'FontName','Times');
grid on;
axis([1 100 1e-16 1]);
set(gca,'FontSize',16,'FontName','Times');
hold on;
figure(3);
ylabel('Estimated number of iterations','FontWeight','demi','FontSize',18,'FontName','Times');
xlabel('Number of processors','FontWeight','demi','FontSize',18,'FontName','Times');
grid on;
axis([4 5000 0 11]);
set(gca,'FontSize',16,'FontName','Times');
hold on;
figure(4);
ylabel('Estimated number of iterations','FontWeight','demi','FontSize',18,'FontName','Times');
xlabel('Sub-domain size','FontWeight','demi','FontSize',18,'FontName','Times');
grid on;
axis([1 100 0 11]);
set(gca,'FontSize',16,'FontName','Times');
hold on;

% analyze the aperiodic nonlinear CRWENO5 scheme using a wave packet
style=['ko--','k^--','kv--','ks--','kd--','kh--','kp--'];
ns1=4;
ns2=ns1-1;
for j=1:size(D,2)
    d = D(j);
    nglobal = 2^d;
    nprocs  = 2.^(max(2,d-6):d-2);
    ndata   = size(nprocs,2);
    rho     = zeros(ndata,1);
    niter   = zeros(ndata,1);
    nlocal  = zeros(ndata,1);
    for i = 1:ndata
        nlocal(i) = nglobal/nprocs(i);
        fprintf('%4d %4d %4d\n',nglobal,nprocs(i),nlocal(i));
        % generate a function to calculate weights with
        [U,x] = DiscontinuousWaves(nlocal(i),nprocs(i));
%         % plot this function
%         figure(5);
%         plot(x,U,'-k');
        % construct the CRWENO5 tridiagonal scheme
        [A,B,C]= CRWENO5(nlocal(i),nprocs(i),0,U);
        % compute the reduced system
        R = TridiagLU(A,B,C,nlocal(i),nprocs(i));
        % construct the iteration matrix for Jacobi iterations
        G = eye(nprocs(i)-1)-diag(1./diag(R),0)*R;
        % calculate max eigenvalue
        rho(i) = max(abs(eig(G)));
        % calculate convergence rate
        rate = -log10(rho(i));
        % estimate number of iterations for machine-zero conv
        niter(i) = int16(conv_crit/rate);
    end
    figure(1);
    loglog(nprocs,rho,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
           'MarkerSize',15);
    hold on;
    figure(2);
    loglog(nlocal,rho,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
           'MarkerSize',15);
    hold on;
    figure(3);
    semilogx(nprocs,niter,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
             'MarkerSize',15);
    hold on;
    figure(4);
    semilogx(nlocal,niter,style(ns1*j-ns2:ns1*j),'linewidth',2, ...
             'MarkerSize',15);
    hold on;
end

% analyze the aperiodic nonlinear CRWENO5 scheme using random-phased
% sum-of-sine waves
nrealizations = 1000;
style=['ko:','k^:','kv:','ks:','kd:','kh:','kp:'];
ns1=3;
ns2=ns1-1;
shadecolor = [0.8 0.8 0.8];
for j=1:size(D,2)
    d = D(j);
    nglobal   = 2^d;
    nprocs    = 2.^(max(2,d-6):d-2);
    ndata     = size(nprocs,2);
    rho_avg   = zeros(ndata,1);
    rho_max   = zeros(ndata,1);
    rho_min   = zeros(ndata,1);
    rho_err   = zeros(ndata,1);
    niter_avg = zeros(ndata,1);
    niter_min = zeros(ndata,1);
    niter_max = zeros(ndata,1);
    niter_err = zeros(ndata,1);
    nlocal    = zeros(ndata,1);
    for i = 1:ndata
        nlocal(i) = nglobal/nprocs(i);
        fprintf('%4d %4d %4d\n',nglobal,nprocs(i),nlocal(i));
        rho = zeros(nrealizations,1);
        for k=1:nrealizations
            % generate a function to calculate weights with
            [U,x] = SumOfSines(nlocal(i),nprocs(i));
%             % plot this function
%             figure(6);
%             plot(x,U,'-k');
            % construct the CRWENO5 tridiagonal scheme
            [A,B,C]= CRWENO5(nlocal(i),nprocs(i),0,U);
            % compute the reduced system
            R = TridiagLU(A,B,C,nlocal(i),nprocs(i));
            % construct the iteration matrix for Jacobi iterations
            G = eye(nprocs(i)-1)-diag(1./diag(R),0)*R;
            % calculate max eigenvalue
            rho(k) = max(abs(eig(G)));
        end
        rho_avg(i) = sum(rho) / nrealizations;
        rho_max(i) = max(rho);
        rho_min(i) = min(rho);
        % calculate convergence rate
        rate = -log10(rho_avg(i));
        niter_avg(i) = int16(conv_crit/rate);
        rate = -log10(rho_max(i));
        niter_max(i) = int16(conv_crit/rate);
        rate = -log10(rho_min(i));
        niter_min(i) = int16(conv_crit/rate);
        rho_err(i)  = max(rho_max(i)-rho_avg(i),rho_avg(i)-rho_min(i));
        niter_err(i)= max(niter_max(i)-niter_avg(i), ...
                          niter_avg(i)-niter_min(i));
        if (niter_err(i) == 0) 
            niter_err(i) = 1;
        end
    end
    figure(1);
    loglog(nprocs,rho_avg,style(ns1*j-ns2:ns1*j),'linewidth',4, ...
           'MarkerSize',15);
    hold on;
    plotshaded(nprocs,[rho_min,rho_max],shadecolor);
    hold on;
    figure(2);
    loglog(nlocal,rho_avg,style(ns1*j-ns2:ns1*j),'linewidth',4, ...
           'MarkerSize',15);
    hold on;
    nlocal2 = nlocal(ndata:-1:1);
    rho_min2 = rho_min(ndata:-1:1);
    rho_max2 = rho_max(ndata:-1:1);
    plotshaded(nlocal2',[rho_min2,rho_max2],shadecolor);
    hold on;
    figure(3);
    semilogx(nprocs,niter_avg,style(ns1*j-ns2:ns1*j),'linewidth',4, ...
             'MarkerSize',15);
    hold on;
    plotshaded(nprocs,[niter_min,niter_max],shadecolor);
    hold on;
    figure(4);
    semilogx(nlocal,niter_avg,style(ns1*j-ns2:ns1*j),'linewidth',4, ...
             'MarkerSize',15);
    hold on;
    niter_min2 = niter_min(ndata:-1:1);
    niter_max2 = niter_max(ndata:-1:1);
    plotshaded(nlocal2',[niter_min2,niter_max2],shadecolor);
    hold on;
end

figure(1);
hold off;
figure(2);
hold off;
figure(3);
hold off;
figure(4);
hold off;

handle = figure(1);
print(handle,'-depsc2','Figure1.eps');
handle = figure(2);
print(handle,'-depsc2','Figure2.eps');
handle = figure(3);
print(handle,'-depsc2','Figure3.eps');
handle = figure(4);
print(handle,'-depsc2','Figure4.eps');

handle = figure(1);
saveas(handle,'Figure1','fig');
handle = figure(2);
saveas(handle,'Figure2','fig');
handle = figure(3);
saveas(handle,'Figure3','fig');
handle = figure(4);
saveas(handle,'Figure4','fig');
