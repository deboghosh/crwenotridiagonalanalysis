function [A,B,C] = CRWENO5Periodic(nlocal,nproc,flag)
%CRWENO5: Function to create the left-hand side tridiagonal
%         matrix for the CRWENO5 scheme
%
% Input arguments:
%   nlocal: local number of points on each processor
%   nproc : number of processors
%   flag  : if 1, use optimal weights, if 0, use random weights


A = zeros(nlocal,nproc);
B = zeros(nlocal,nproc);
C = zeros(nlocal,nproc);

% optimal coefficients
c1 = 0.2;
c2 = 0.5;
c3 = 0.3;

% generating random values between 0 and 1
random1 = rand(nlocal+1,nproc);
random2 = rand(nlocal+1,nproc);

% allocate arrays for the tridiagonal system of the interface values
alpha = zeros(nlocal+1,nproc);
beta  = zeros(nlocal+1,nproc);
gamma = zeros(nlocal+1,nproc);

% create the CRWENO5 interface tridiagonal system
for rank = 1:nproc
    for j=1:nlocal+1
        % setting WENO weights
        if (flag) 
            omega1 = c1;
            omega2 = c2;
            omega3 = c3;
        else
            omega1 = random1(j,rank);
            omega2 = random2(j,rank);
            omega3 = 1.0 - omega1 - omega2;
        end
        % left-hand side coefficients of CRWENO5 scheme
        alpha(j,rank) = (2.0/3.0)*omega1 + (1.0/3.0)*omega2;
        beta (j,rank) = (1.0/3.0)*omega1 + (2.0/3.0)*(omega2+omega3);
        gamma(j,rank) = (1.0/3.0)*omega3;
    end
end
% ensuring consistency for interfaces that abut parallel subdomain
% boundaries
for rank = 1:nproc-1
    alpha(nlocal+1,rank) = alpha(1,rank+1);
    beta (nlocal+1,rank) = beta (1,rank+1);
    gamma(nlocal+1,rank) = gamma(1,rank+1);
end

% create the derivative tridiagonal system
for rank = 1:nproc
    for j = 1:nlocal
        A(j,rank) = alpha(j,rank);
        B(j,rank) = beta(j,rank)+alpha(j,rank)-alpha(j+1,rank);
        C(j,rank) = gamma(j,rank)+beta(j,rank)+alpha(j,rank) ...
                    -beta(j+1,rank)-alpha(j+1,rank);
    end
end

%done
end

