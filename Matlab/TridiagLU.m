function r = TridiagLU(a,b,c,n,p)
% TRIDIAGLU: Function to simulate the parallel LU
% decomposition of a tridiagonal system defined by
% [A,B,C] with n number of points (per processor)
% and p processors.
%
% Input Arguments:
%   n: number of points per processor
%   p: number of processors
%   a: sub-diagonal elements   (n x p)
%   b: diagonal elements       (n x p)
%   c: super-diagonal elements (n x p)
%
% Output:
%   r: the reduced system resulting from the LU decomposition
%      (matrix of size p-1 x p-1)

% Stage 1: Embarassingly parallel elimination of interior points
for rank=1:p
    if (rank==1)
        istart = 2;
    else
        istart = 3;
    end
    iend = n;
    for j=istart:iend
        factor = a(j,rank)/b(j-1,rank);
        b(j,rank) = b(j,rank) - (factor * c(j-1,rank));
        a(j,rank) = -factor * a(j-1,rank);
        if (rank > 1)
            factor = c(1,rank) / b(j-1,rank);
            c(1,rank) = -factor * c(j-1,rank);
            b(1,rank) = b(1,rank) - factor * a(j-1,rank);
        end
    end
end

% Stage 2: Eliminate first row on each rank except the first one
for rank=2:p
    am1 = a(n,rank-1);
    bm1 = b(n,rank-1);
    cm1 = c(n,rank-1);
    
    factor = a(1,rank) / bm1;
    b(1,rank) = b(1,rank) - factor * cm1;
    a(1,rank) = -factor * am1;
    
    factor = c(1,rank) / b(n,rank);
    b(1,rank) = b(1,rank) - factor * a(n,rank);
    c(1,rank) = -factor * c(n,rank);
end

% Stage 3: Assemble the reduced system
r = zeros(p-1,p-1);
r(1,1) = b(1,2);
r(1,2) = c(1,2);
for rank=3:p-1
    r(rank-1,rank-1) = b(1,rank);
    r(rank-1,rank-2) = a(1,rank);
    r(rank-1,rank  ) = c(1,rank);
end
r(p-1,p-2) = a(1,p);
r(p-1,p-1) = b(1,p);

%done
end

