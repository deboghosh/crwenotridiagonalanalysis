function [U,x] = SumOfSines(nlocal,nproc)
%SUMOFSINES Generates a function that is the sum of sine waves
%   Input arguments:
%       nlocal: number of points on each processor
%       nproc : number of processors

%   Output arguments:
%       U: the generated function with dimensions (nglobal+2*ghosts,nproc)

% some constants
ghosts = 3;

% calculate global domain size
nglobal = nlocal*nproc;

% maximum wavenumber
max_wavenumber = nglobal/2;

% allocate global x and U arrays
x = zeros(nglobal+2*ghosts,1);
U = zeros(nglobal+2*ghosts,1);

% calculate dx and set x (grid)
dx = 1.0 / (nglobal-1);
for i=1:(nglobal+2*ghosts)
    x(i) = (i-1-ghosts)*dx - 0.5;
end

% generate random phases
phase = rand(max_wavenumber,1)*2*pi;

% calculate U
for i=1:(nglobal+2*ghosts)
    U(i) = 0.0;
    for k=1:max_wavenumber
        Ak = k^(-5.0/6.0);
        U(i) = U(i) + Ak*cos(2*pi*k*x(i)+phase(k));
    end
end

end

