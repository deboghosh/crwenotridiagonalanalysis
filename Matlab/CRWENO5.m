function [A,B,C] = CRWENO5(nlocal,nproc,flag,U)
%CRWENO5: Function to create the left-hand side tridiagonal
%         matrix for the CRWENO5 scheme
%
% Input arguments:
%   nlocal: local number of points on each processor
%   nproc : number of processors
%   flag  : if 1, use optimal weights, if 0, use random weights
%   U     : a function, U(x), based on which the weights are computed

A = zeros(nlocal,nproc);
B = zeros(nlocal,nproc);
C = zeros(nlocal,nproc);

% optimal coefficients
c1 = 0.2;
c2 = 0.5;
c3 = 0.3;

% some constants
eps    = 0.000001;
ghosts = 3;

% finding local u from global U
u = zeros(nlocal+2*ghosts,nproc);
for rank=1:nproc
    for j=1:(nlocal+2*ghosts)
        u(j,rank) = U((rank-1)*nlocal+j);
    end
end

% allocate arrays for the tridiagonal system of the interface values
alpha = zeros(nlocal+1,nproc);
beta  = zeros(nlocal+1,nproc);
gamma = zeros(nlocal+1,nproc);

% create the CRWENO5 interface tridiagonal system
for rank = 1:nproc
    for j=1:nlocal+1
        % setting WENO weights
        if (flag)
            % use optimal values
            omega1 = c1;
            omega2 = c2;
            omega3 = c3;
        else
            % calculate based on u(x)
            m3 = u(j-3+ghosts,rank);
            m2 = u(j-2+ghosts,rank);
            m1 = u(j-1+ghosts,rank);
            p1 = u(j  +ghosts,rank);
            p2 = u(j+1+ghosts,rank);
            b1 = 13.0/12.0*(m3-2*m2+m1)^2 + 1.0/4.0*(m3-4*m2+3*m1)^2;
            b2 = 13.0/12.0*(m2-2*m1+p1)^2 + 1.0/4.0*(m2-p1)^2;
            b3 = 13.0/12.0*(m1-2*p1+p2)^2 + 1.0/4.0*(3*m1-4*p1+p2)^2;
            tau = (m3-4*m2+6*m1-4*p1+p2)^2;
            a1 = c1 * (1.0 + (tau/(b1+eps))^2);
            a2 = c2 * (1.0 + (tau/(b2+eps))^2);
            a3 = c3 * (1.0 + (tau/(b3+eps))^2);
            omega1 = a1 / (a1+a2+a3);
            omega2 = a2 / (a1+a2+a3);
            omega3 = a3 / (a1+a2+a3);
        end
        % left-hand side coefficients of CRWENO5 scheme
        alpha(j,rank) = (2.0/3.0)*omega1 + (1.0/3.0)*omega2;
        beta (j,rank) = (1.0/3.0)*omega1 + (2.0/3.0)*(omega2+omega3);
        gamma(j,rank) = (1.0/3.0)*omega3;
    end
end
% Use WENO5 at physical boundaries
alpha(1,1) = 0.0;
beta (1,1) = 1.0;
gamma(1,1) = 0.0;
alpha(nlocal+1,nproc) = 0.0;
beta (nlocal+1,nproc) = 1.0;
gamma(nlocal+1,nproc) = 0.0;
% ensuring consistency for interfaces that abut parallel subdomain
% boundaries
for rank = 1:nproc-1
    alpha(nlocal+1,rank) = alpha(1,rank+1);
    beta (nlocal+1,rank) = beta (1,rank+1);
    gamma(nlocal+1,rank) = gamma(1,rank+1);
end

% create the derivative tridiagonal system
for rank = 1:nproc
    for j = 1:nlocal
        A(j,rank) = alpha(j,rank);
        B(j,rank) = beta(j,rank)+alpha(j,rank)-alpha(j+1,rank);
        C(j,rank) = gamma(j,rank)+beta(j,rank)+alpha(j,rank) ...
                    -beta(j+1,rank)-alpha(j+1,rank);
    end
end

%done
end

